#include "text_answer.hpp"
#include <algorithm>
#include <sstream>
#include <fstream>

void text_answer::create(Page* pg)
{
    string input;
    string option;
    string changing_option;
    string next_page;
    bool pass = false;
    this_page = pg;

    this->setAtributes();
    if (scored)
    {
        cout << "Zadejte spravnou moznost: ";
        while (1)
        {
            getline(cin, option);
            if (cin.eof()) throw "\nPredcasne ukonceny vstup";
            if (option == "" && correct_options.empty())
            {
                cout << "Povinne pole, zadejte spravnou moznost: ";
                continue;
            }
            cout << "Zadali jste:" << "\"" << option << "\"" << endl;
            cout << "Pokud si prejete zmenit zneni odpovedi, napiste zmenit, pokud jste spokojeni, stisknete enter: ";
            while (1)
            {
                getline(cin, input);
                if (cin.eof()) throw "\nPredcasne ukonceny vstup";
                if (input == "zmenit")
                {
                    cout << "Zadejte novou odpoved: ";
                    break;
                }
                if (input == "")
                {
                    break;
                }
                if (input != "" || input != "zmenit")
                {
                    cout << "Nespravny vstup, vynechejte nebo zadejte 'zmenit': ";
                    continue;
                }
            }
            if (input == "")
            {
                correct_options.insert(option);
            }
            cout << "Pokud chcete zadat dalsi spravnou odpoved, stisknete enter pokud ne, napiste 'konec': ";
            while (1)
            {
                getline(cin, input);
                if (cin.eof())
                {
                    throw "\nPredcasne ukonceny vstup";
                }
                transform(input.begin(), input.end(), input.begin(), ::tolower);
                if (input == "")
                {
                    pass = false;    
                    break;
                } else if (input == "konec")
                {
                    pass = true;
                    break;
                } else {
                    cout << "Nespravny vstup, zadejte 'konec', nebo stisknete enter: ";
                    continue;
                }
            }
            if (pass) break;
        }
    }
    cout << "Meni tato otazka poradi stranek? (zadejte ano/ne): ";
    while (1)
    {
        getline(cin, input);
        if (cin.eof()) throw "\nPredcasne ukonceny vstup";
        transform(input.begin(), input.end(), input.begin(), ::toupper);
        if (input == "ANO" || input == "PRAVDA" || input == "JO" || input == "A")
        {
            cout << "Zadejte vstup, ktery zmeni nasledujici stranu: ";
            while (1)
            {
                getline(cin, changing_option);
                if (cin.eof()) throw "\nPredcasne ukonceny vstup";
                if (changing_option == "")
                {
                    cout << "Konec zadavani odpovedi menicich nasledujici stranu, pokracuji..." << endl;
                    break;
                }
                cout << "Zadali jste:" << "\"" << changing_option << "\"" << endl;
                cout << "Pokud si prejete zmenit zneni odpovedi, napiste zmenit, pokud jste spokojeni, stisknete enter: ";
                while (1)
                {
                    getline(cin, input);
                    if (cin.eof()) throw "\nPredcasne ukonceny vstup";
                    if (input == "zmenit")
                    {
                        cout << "Zadejte vstup, ktery zmeni nasledujici stranu: ";
                        break;
                    }
                    if (input == "")
                    {
                        break;
                    }
                    if (input != "" || input != "zmenit")
                    {
                        cout << "Nespravny vstup, vynechejte nebo zadejte 'zmenit': ";
                        continue;
                    }
                }
                cout << "Zadejte nazev strany, ktera ma nasledovat po teto odpovedi: ";
                while (1)
                {
                    getline(cin, next_page);
                    if (cin.eof()) throw "\nPredcasne ukonceny vstup";
                    if (next_page == "")
                    {
                        cout << "Povinne pole, zadejte nazev strany: ";
                        continue;
                    }
                    cout << "Zadali jste:" << "\"" << next_page << "\"" << endl;
                    cout << "Pokud si prejete zmenit nazev strany, napiste zmenit, pokud jste spokojeni, stisknete enter: ";
                    while (1)
                    {
                        getline(cin, input);
                        if (cin.eof()) throw "\nPredcasne ukonceny vstup";
                        if (input == "zmenit")
                        {
                            cout << "Zadejte vstup, ktery zmeni nasledujici stranu: ";
                            break;
                        }
                        if (input == "")
                        {
                            break;
                        }
                        if (input != "" || input != "zmenit")
                        {
                            cout << "Nespravny vstup, vynechejte nebo zadejte 'zmenit': ";
                            continue;
                        }
                    }
                    if (input == "")
                    {
                        break;
                    }
                }
                if (input == "")
                {
                    change_options.insert(make_pair(changing_option, next_page));
                    cout << "Zadejte dalsi vstup, ktery meni nasledujici stranu, nebo stisknete enter, pokud dalsi neni: ";
                    continue;
                }
            }
        } else if (input == "NE" || input == "NEPRAVDA" || input == "LEZ" || input == "N")
        {
            break;
        } else
        {
            cout << "Nespravny vstup, zadejte ano/ne: ";
            continue;
        }
        break;
    }
}

void text_answer::printQuestion() const
{
    if (Text == "") throw "\nNeplatna otazka.";
    cout << "\t\tOtazka: " << Text << endl;
}

int text_answer::answer(string& next)
{
    user_answer = "";
    this->printQuestion();
    string answer;
    bool passed = false;
    cout << "\t\tOdpoved: ";
    while (1)
    {
        getline(cin, answer);
        if (cin.eof()) throw "\nPredcasne ukonceny vstup.";
        if (answer.length() == 0)
        {
            if (mandatory)
            {
                cout << "Otazka je povinna, musite zadat odpoved: ";
                continue;
            } else
            {           
                cout << "Preskakuji odpoved..." << endl;
                passed = true;
                break;
            }
        }
        user_answer = answer;
        break;
    }
    if (!passed)
    {
        auto changeIt = change_options.find(user_answer);
        if (changeIt != change_options.end())
        {
            next = changeIt->second;
        } else next = "";
    } else next = "";
    if (scored && !passed)
    {
        for (auto &v : correct_options)
        {
            if (answer == v) 
            {
                cout << "\t\tSpravne, ziskavate " << plus_points << " bodu." << endl;
                return plus_points;
            }
        }
        cout << "\t\tSpatne, ziskavate " << minus_points << " bodu." << endl;
        return minus_points;
    }
    return 0;
}

bool text_answer::importQuestion(ifstream& src, Page* pg)
{
    if (!importAtributes(src, pg)) return false;
    string input;
    string input_changing_page;
    if (scored)
    {
        getline(src, input);
        transform(input.begin(), input.end(), input.begin(), ::tolower);
        if (input != "spravne moznosti:")
        {
            cout << "Chybi spravna moznost u otazky \"" << Text << "\"" << endl;
            cout << "Nacteno: " << input << endl;
            return false;
        }
        getline(src, input);
        transform(input.begin(), input.end(), input.begin(), ::tolower);
        if (input == "")
        {
            cout << "Chybi spravna moznost u otazky \"" << Text << "\"" << endl;
            return false;
        }
        correct_options.insert(input);
        while (1)
        {
            getline(src, input);
            string input_backup = input;
            transform(input.begin(), input.end(), input.begin(), ::tolower);
            if (input == "meni poradi stran?")
            {
                break;
            }
            if (input == "")
            {
                cout << "Prazdna spravna moznost u otazky \"" << Text << "\"" << endl;
                return false;
            }
            correct_options.insert(input_backup);
        }
    } else
    {
        getline(src, input);
        transform(input.begin(), input.end(), input.begin(), ::tolower);
        if (input != "meni poradi stran?")
        {
            cout << "Chybi zmena poradi stran u otazky \"" << Text << "\"" << endl;
            cout << "Nacteno: " << input << endl;
            return false;
        }
    }
    getline(src, input);
    transform(input.begin(), input.end(), input.begin(), ::tolower);
    if (input == "ano")
    {
        while (1)
        {
            getline(src, input);
            transform(input.begin(), input.end(), input.begin(), ::tolower);
            if (input == "konec otazky")
            {
                getline(src, input);
                if (input != "")
                {
                    cout << "Chybi prazdna radka za otazkou \"" << Text << "\"" << endl;
                    cout << "Nacteno: " << input << endl;
                    return false;
                }
                return true;
            }
            if (input != "vstup:")
            {
                cout << "Chybi urceni vstupu meniciho poradi stran u otazky \"" << Text <<  "\"" << endl;
                return false;
            }
            getline(src, input);
            if (input == "")
            {
                cout << "Chybi urceni vstupu meniciho poradi stran u otazky \"" << Text <<  "\"" << endl;
                return false;
            }
            input_changing_page = input;
            getline(src, input);
            transform(input.begin(), input.end(), input.begin(), ::tolower);
            if (input != "strana:")
            {
                cout << "Chybi nazev nasledujici strany u otazky \"" << Text <<  "\"" << endl;
                return false;
            }
            getline(src, input);
            if (input == "")
            {
                cout << "Chybi nazev nasledujici strany u otazky \"" << Text <<  "\"" << endl;
                return false;
            }
            change_options.insert(make_pair(input_changing_page, input));
        }
    } else if (input == "ne")
    {
        getline(src, input);
        transform(input.begin(), input.end(), input.begin(), ::tolower);
        if (input != "konec otazky")
        {
            cout << "Chybi konec otazky u otazky \"" << Text << "\"" << endl;
            cout << "Nacteno: " << input << endl;
            return false;
        }
        getline(src, input);
        transform(input.begin(), input.end(), input.begin(), ::tolower);
        if (input != "")
        {
            cout << "Chybi prazdna radka za otazkou \"" << Text << "\"" << endl;
            cout << "Nacteno: " << input << endl;
            return false;
        }
    } else
    {
        cout << "Nespravna moznost na poradi stran u otazky \"" << Text << "\"" << endl;
        cout << "Nacteno: " << input << endl;
        return false;
    }
    
    return true;
}

void text_answer::exportQuestion(ofstream& outFile) const
{
    outFile << "Typ otazky:" << endl;
    outFile << "textova odpoved" << endl;
    this->exportAtributes(outFile);
    if (scored)
    {
        outFile << "Spravne moznosti:" << endl;
        for (auto &v : correct_options)
        {
            outFile << v << endl;
        }
    }
    outFile << "Meni poradi stran?" << endl;
    if (!(change_options.empty()))
    {
        outFile << "ano" << endl;
    } else outFile << "ne" << endl;
    if (!(change_options.empty()))
    {
        for (auto &v : change_options)
        {
            outFile << "Vstup:" << endl;
            outFile << v.first << endl;
            outFile << "Strana:" << endl;
            outFile << v.second << endl;
        }
    }
    outFile << "konec otazky" << endl << endl;
}