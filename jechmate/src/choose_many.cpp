#include "choose_many.hpp"
#include <algorithm>
#include <sstream>
#include <fstream>

void choose_many::create(Page* pg)
{
    string input;
    string type = "choose_many";
    set<char> input_changing_page;
    string next_page_name;
    string option_text;
    char correct_option;
    bool pass = false;
    bool incorrect = false;
    Option opt;
    char option = 'A';
    this_page = pg;

    this->setAtributes();
    cout << "Zadejte moznosti:" << endl;
    while (1)
    {
        incorrect = false;
        cout << "Moznost " << option << ": ";
        getline(cin, option_text);
        if (cin.eof()) throw "\nPredcasne ukonceny vstup";
        if (option_text == "")
        {
            pass = true;
        } else
        {
            for (auto &v : options)
            {
                if (v.getText() == option_text)
                {
                    incorrect = true;
                    break;
                }
            }
        }
        if (incorrect)
        {
            cout << "Tato moznost jiz existuje, zadejte prosim novou." << endl;
            continue;
        }
        if (!pass)
        {
            opt.create(option, option_text, this, type);
        }
        if (pass || option == 'Z')
        {
            if (pass) option--;
            if (option == 'Z') options.push_back(Option(opt));
            if (scored)
            {
                cout << "Zadejte spravne moznosti oddelene mezerami (A-" << option << " nebo 'vse' nebo 'nic'): ";
                while (1)
                {
                    getline(cin, input);
                    if (cin.eof()) throw "\nPredcasne ukonceny vstup";
                    if (input == "")
                    {
                        cout << "Nespravny vstup, zadejte (A-" << option << " nebo 'vse' nebo 'nic'): ";
                        continue; 
                    }
                    transform(input.begin(), input.end(), input.begin(), ::toupper);
                    if (input == "VSE")
                    {
                        for (char all_option = 'A'; all_option <= option; ++all_option)
                        {
                            correct_options.insert(all_option);
                        }
                        break;
                    } else if (input == "NIC")
                    {
                        break;
                    }
                    stringstream ss(input);
                    string token;
                    while(getline(ss, token, ' '))
                    {
                        if (token.length() != 1)
                        {
                            cout << "Nespravny vstup, zadejte moznosti oddelene mezerami (A-" << option << "): ";
                            incorrect = true;
                            break;
                        }
                        correct_option = token[0];
                        if (correct_option < 'A' || (correct_option > option ))
                        {
                            cout << "Nespravny vstup, zadejte (A-" << option << "): ";
                            incorrect = true;
                            break;
                        }
                        correct_options.insert(correct_option);
                    }
                    if (incorrect){
                        incorrect = false;
                        continue;
                    }
                    break;
                }
            }
            break;
        } else 
        {
            options.push_back(Option(opt));
            pass = false;
            option++;
        }
    }
    cout << "Zadejte kombinaci, ktera zmeni nasledujici stranu, vynechejte, pokud takova neni, nebo napiste 'vse' nebo 'nic': ";
    while (1)
    {
        incorrect = false;
        getline(cin, input);
        if (cin.eof()) throw "\nPredcasne ukonceny vstup";
        if (input == "") break;
        transform(input.begin(), input.end(), input.begin(), ::toupper);
        if (input == "VSE")
        {
            for (char all_option = 'A'; all_option <= option; ++all_option)
            {
                input_changing_page.insert(all_option);
            }
        } else if (input == "NIC")
        {
            input_changing_page.clear();
        } else
        {
            stringstream ss(input);
            string token;
            while(getline(ss, token, ' '))
            {
                if (token.length() != 1)
                {
                    cout << "Nespravny vstup, zadejte moznosti oddelene mezerami (A-" << option << "): ";
                    incorrect = true;
                    break;
                }
                correct_option = token[0];
                if (correct_option < 'A' || (correct_option > option ))
                {
                    cout << "Nespravny vstup, zadejte (A-" << option << "): ";
                    incorrect = true;
                    break;
                }
                input_changing_page.insert(correct_option);
            }
            if (incorrect){
                incorrect = false;
                continue;
            }
        }
        cout << "Zadejte jmeno stranky, ktera ma nasledovat po zadani teto odpovedi: ";
        while (1)
        {
            getline(cin, next_page_name);
            if (cin.eof()) throw "\nPredcasne ukonceny vstup";
            if (next_page_name == "")
            {
                cout << "Povinne pole, zadejte nazev stranky: ";
                continue;
            }
            cout << "Zadali jste:" << "\"" << next_page_name << "\"" << endl;
            cout << "Pokud si prejete zmenit jmeno stranky, napiste 'zmenit', stisknete enter pokud jste spokojeni: ";
            while (1)
            {
                getline(cin, input);
                if (cin.eof()) throw "\nPredcasne ukonceny vstup";
                if (input == "zmenit")
                {
                    cout << "Zadejte nove jmeno stranky: ";
                    break;
                }
                if (input == "")
                {
                    break;
                }
                if (input != "" || input != "zmenit")
                {
                    cout << "Nespravny vstup, vynechejte nebo zadejte 'zmenit': ";
                    continue;
                }
            }
            if (input == "") 
            {
                change_page.insert(make_pair(input_changing_page, next_page_name));
                break;
            }
        }
        cout << "Zadejte dalsi kombinaci, nebo stisknete enter pro pokracovani: ";
    }
}

void choose_many::printQuestion() const
{
    if (Text == "") throw "\nNeplatna otazka.";
    cout << "\t\tOtazka: " << Text << endl;
    for (auto &v : options)
    {
        cout << v;
    }
}

int choose_many::answer(string& next)
{
    user_answer.clear();
    string input;
    char maxOption = options.size() + 64;
    char input_option;
    if (maxOption <= 65 || maxOption > 90) throw "\nNeplatna otazka.";
    bool incorrect = false;
    bool empty_answer = false;
    bool passed = false;

    this->printQuestion();
    cout << "\t\tOdpoved (zadejte znaky A-" << maxOption << " oddelene mezerami, 'vse' nebo 'nic'): ";
    while (1)
    {
        getline(cin, input);
        if (cin.eof()) throw "\nPredcasne ukonceny vstup";
        if (input.length() == 0)
        {
            if (mandatory)
            {
                cout << "Otazka je povinna, musite zadat odpoved: ";
                continue;
            } else
            {           
                cout << "Preskakuji odpoved..." << endl;
                passed = true;
                break;
            }
        }
        transform(input.begin(), input.end(), input.begin(), ::toupper);
        if (input == "VSE")
        {
            for (char all_option = 'A'; all_option <= maxOption; ++all_option)
            {
                user_answer.insert(all_option);
            }
            break;
        } else if (input == "NIC")
        {
            user_answer.clear();
            empty_answer = true;
            break;
        }
        stringstream ss(input);
        string token;
        while(getline(ss, token, ' '))
        {
            if (token.length() != 1)
            {
                cout << "\t\tNespravny vstup, zadejte moznosti oddelene mezerami (A-" << maxOption << "): ";
                incorrect = true;
                break;
            }
            input_option = token[0];
            if (input_option < 'A' || (input_option > maxOption ))
            {
                cout << "\t\tNespravny vstup, zadejte (A-" << maxOption << "): ";
                incorrect = true;
                break;
            }
            user_answer.insert(input_option);
        }
        if (incorrect){
            incorrect = false;
            continue;
        }
        break;
    }
    if (!passed)
    {
        auto changeIt = change_page.find(user_answer);
        if (changeIt != change_page.end())
        {
            next = changeIt->second;
        } else next = "";
    } else
    {
        next = "";
        return 0;
    }
    if (scored && !passed)
    {
        if (empty_answer && correct_options.empty())
        {
            cout << "\t\tSpravne, ziskavate " << plus_points << " bodu." << endl;
            return plus_points;
        }
        if (user_answer == correct_options)
        {
            cout << "\t\tSpravne, ziskavate " << plus_points << " bodu." << endl;
            return plus_points;
        } else
        {
            cout << "\t\tSpatne, ziskavate " << minus_points << " bodu." << endl;
            return minus_points;
        }
    }
    return 0;
}

bool choose_many::importQuestion(ifstream& src, Page* pg)
{
    if (!importAtributes(src, pg)) return false;
    string input;
    char position = 'A';
    set<char> input_changing_page;
    char correct_option;
    string option_text;
    string next_page;
    bool skip = false;

    getline(src, input);
    transform(input.begin(), input.end(), input.begin(), ::tolower);
    if (input != "moznosti:")
    {
        cout << "Chybi vypis moznosti u otazky \"" << Text << "\"" << endl;
        cout << "Nacteno: " << input << endl;
        return false;
    }
    while (1)
    {
        getline(src, input);
        transform(input.begin(), input.end(), input.begin(), ::tolower);
        if (input == "konec moznosti")
        {
            if (position > 'B') break;
            else
            {
                cout << "Otazka \"" << Text << "\" ma mene nez dve moznosti." << endl;
                return false;
            }
        }
        if ( position == ('Z' + 1))
        {
            cout << "Otazka \"" << Text << "\" ma prilis mnoho moznosti." << endl;
            return false;
        }
        if (input != "text:")
        {
            cout << "Chybi text moznosti u otazky \"" << Text << "\"" << endl;
            cout << "Nacteno: " << input << endl;
            return false;
        }
        getline(src, input);
        if (input == "")
        {
            cout << "Chybi text moznosti u otazky \"" << Text << "\"" << endl;
            cout << "Nacteno: " << input << endl;
            return false;
        }
        for (auto &v : options)
        {
            if (v.getText() == input)
            {
                skip = true;
                break;
            }
        }
        option_text = input;
        if (skip)
        {
            cout << "Zadana stejna moznost vicekrat, preskakuji ji." << endl;
            skip = false;
            continue;
        } else
        {
            options.push_back(Option(position, option_text, next_page, this));
            position++;   
        }
    }
    position--;
    if (scored)
    {
        getline(src, input);
        transform(input.begin(), input.end(), input.begin(), ::tolower);
        if (input != "spravna odpoved:")
        {
            cout << "Chybi vypis spravne odpovedi u otazky \"" << Text << "\"" << endl;
            cout << "Nacteno: " << input << endl;
            return false;
        }
        getline(src, input);
        if (input == "")
        {
            cout << "Nespravny vstup spravne odpovedi u otazky \"" << Text <<  "\"" << endl;
            return false; 
        }
        transform(input.begin(), input.end(), input.begin(), ::toupper);
        if (input == "VSE")
        {
            for (char all_option = 'A'; all_option <= position; ++all_option)
            {
                correct_options.insert(all_option);
            }
        } else if (input == "NIC")
        {
        } else 
        {
            stringstream ss(input);
            string token;
            while(getline(ss, token, ' '))
            {
                if (token.length() != 1)
                {
                    cout << "Spravna moznost ma vice nez jeden znak u otazky \"" << Text <<  "\"" << endl;
                    return false;
                }
                correct_option = token[0];
                if (correct_option < 'A' || (correct_option > position ))
                {
                    cout << "Nespravny znak odpovedi u otazky \"" << Text <<  "\"" << endl;
                    return false;
                }
                correct_options.insert(correct_option);
            }
        }
    }
    getline(src, input);
    transform(input.begin(), input.end(), input.begin(), ::tolower);
    if (input != "meni poradi stran?")
    {
        cout << "Chybi urceni meneni poradi stran u otazky \"" << Text <<  "\"" << endl;
        return false;
    }
    getline(src, input);
    transform(input.begin(), input.end(), input.begin(), ::tolower);
    if (input == "ano")
    {
        while (1)
        {
            getline(src, input);
            transform(input.begin(), input.end(), input.begin(), ::tolower);
            if (input == "konec otazky")
            {
                getline(src, input);
                if (input != "")
                {
                    cout << "Chybi prazdna radka za otazkou \"" << Text << "\"" << endl;
                    cout << "Nacteno: " << input << endl;
                    return false;
                }
                return true;
            }
            if (input != "kombinace:")
            {
                cout << "Chybi urceni kombinace menici poradi stran u otazky \"" << Text <<  "\"" << endl;
                return false;
            }
            getline(src, input);
            transform(input.begin(), input.end(), input.begin(), ::toupper);
            if (input == "")
            {
                cout << "Chybi urceni kombinace menici poradi stran u otazky \"" << Text <<  "\"" << endl;
                return false;
            }
            if (input == "VSE")
            {
                for (char all_option = 'A'; all_option <= position; ++all_option)
                {
                    input_changing_page.insert(all_option);
                }
            } else if (input == "NIC")
            {
                input_changing_page.clear();
            } else
            {
                stringstream ss(input);
                string token;
                while(getline(ss, token, ' '))
                {
                    if (token.length() != 1)
                    {
                        cout << "Moznost ma vice nez jeden znak u otazky \"" << Text <<  "\"" << endl;
                        return false;
                    }
                    correct_option = token[0];
                    if (correct_option < 'A' || (correct_option > position ))
                    {
                        cout << "Nespravny znak odpovedi u otazky \"" << Text <<  "\"" << endl;
                        return false;
                    }
                    input_changing_page.insert(correct_option);
                }
            }
            getline(src, input);
            transform(input.begin(), input.end(), input.begin(), ::tolower);
            if (input != "nazev strany:")
            {
                cout << "Chybi nazev nasledujici strany u otazky \"" << Text <<  "\"" << endl;
                return false;
            }
            getline(src, input);
            if (input == "")
            {
                cout << "Chybi nazev nasledujici strany u otazky \"" << Text <<  "\"" << endl;
                return false;
            }
            change_page.insert(make_pair(input_changing_page, input));
        }
    } else if (input == "ne")
    {
        getline(src, input);
        transform(input.begin(), input.end(), input.begin(), ::tolower);
        if (input != "konec otazky")
        {
            cout << "Chybi konec otazky u otazky \"" << Text << "\"" << endl;
            cout << "Nacteno: " << input << endl;
            return false;
        }
        getline(src, input);
        transform(input.begin(), input.end(), input.begin(), ::tolower);
        if (input != "")
        {
            cout << "Chybi prazdna radka za otazkou \"" << Text << "\"" << endl;
            cout << "Nacteno: " << input << endl;
            return false;
        }
    } else
    {
        cout << "Nespravne zadani meneni stran u otazky \"" << Text << "\"" << endl;
        cout << "Nacteno: " << input << endl;
        return false;   
    }
    return true;
}

void choose_many::exportQuestion(ofstream& outFile) const
{
    outFile << "Typ otazky:" << endl;
    outFile << "vyber vice" << endl;
    this->exportAtributes(outFile);
    outFile << "Moznosti:" << endl;
    for (auto &v : options)
    {
        outFile << "Text:" << endl;
        outFile << v.getText() << endl;
    }
    outFile << "konec moznosti" << endl;
    if (scored)
    {
        outFile << "Spravna odpoved:" << endl;
        if (correct_options.empty()) outFile << "nic" << endl;
        for (auto &v : correct_options)
        {
            if (v != *(--correct_options.end())) outFile << v << " ";
            else outFile << v << endl;
        }
    }
    outFile << "Meni poradi stran?" << endl;
    if (!(change_page.empty()))
    {
        outFile << "ano" << endl;
    } else outFile << "ne" << endl;
    if (!(change_page.empty()))
    {
        for (auto &v : change_page)
        {
            outFile << "Kombinace:" << endl;
            for (auto &f : v.first)
            {
                if (v.first.empty()) outFile << "nic" << endl;
                else if (f != *(--v.first.end())) outFile << f << " ";
                else outFile << f << endl;
            }
            outFile << "Nazev strany:" << endl;
            outFile << v.second << endl;
        }
    }
    outFile << "konec otazky" << endl << endl;
}