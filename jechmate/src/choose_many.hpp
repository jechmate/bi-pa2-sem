#pragma once
#include "question.hpp"

/**
 * \class choose_many
 * Question where you can choose many answers
 * can be answered by typing in the characters marking the
 * options separated by a space. Can also be answered
 * by typing 'all' or 'none' if all or none of the options are correct
 * */
class choose_many : public Question
{
    public:
        choose_many(){};
        ~choose_many(){};
        choose_many(const choose_many& src){
            this->Text = src.Text;
            this->scored = src.scored;
            this->plus_points = src.plus_points;
            this->minus_points = src.minus_points;
            this->this_page = src.this_page;
            this->options = src.options;
            this->correct_options = src.correct_options;
            this->user_answer = src.user_answer;
        }
        void create(Page*);
        int answer(string&);
        void printQuestion() const;
        bool importQuestion(ifstream&, Page*);
        void exportQuestion(ofstream&) const;

    private:
        map<set<char>,string> change_page; ///< First - Set of chars that when given as an answer change the next Page to the string in Second
        vector<Option> options; ///< Vector of Options given to the user
        set<char> correct_options; ///< Set of chars of the correct Options
        set<char> user_answer; ///< the user's answer
        bool all = false; ///< true if the correct answer is selecting everything
        bool nothing = false; ///< true if the correct answer is selecting nothing
};