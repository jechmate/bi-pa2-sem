#pragma once
#include "question.hpp"

/**
 * \class yes_no
 * Question that has a true/false answer
 * can be answered by typing in A/N, pravda/lez, ano/ne
 * */
class yes_no : public Question
{
    public:
        yes_no(){};
        ~yes_no(){};
        yes_no(const yes_no& src){
            this->Text = src.Text;
            this->scored = src.scored;
            this->plus_points = src.plus_points;
            this->minus_points = src.minus_points;
            this->this_page = src.this_page;
            this->correct_option = src.correct_option;
            this->user_answer = src.user_answer;
        }
        void create(Page*);
        int answer(string&);
        void printQuestion() const;
        bool importQuestion(ifstream&, Page*);
        void exportQuestion(ofstream&) const;

    private:
        bool correct_option; ///< the correct value of the answer
        bool user_answer; ///< answer the user gave
        string next_page_true = ""; ///< the name of a Page to follow if true is selected
        string next_page_false = ""; ///< the name of a Page to follow if false is selected
};