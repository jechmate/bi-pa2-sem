#pragma once
#include <string>
#include <set>
#include <map>
#include <vector>
#include "option.hpp"

using namespace std;

class Page;
/**
 * \class Question
 * Represents one Question that can be answered in a given Page.
 * Can have various types.
 * */
class Question
{
    friend class choose_one;
    friend class Option;
    public:
        Question(){}; ///< default constructor
        virtual ~Question(){}; ///< default destructor
        virtual void create(Page*){}; ///< creates the Question
        void setAtributes(); ///< sets protected atributes that are the same for all types
        /**
         * Imports basic attributes of the Question
         * @param src The stream from which the Question is imported
         * @param pg the Page where the Question lies
         * @return bool Return if the attributes were imported
         * */
        bool importAtributes(ifstream& src, Page* pg);
        /**
         * Imports basic attributes of the Question
         * @param outFile The stream to which the Question is exported
         * */
        void exportAtributes(ofstream& outFile) const;
        /**
         * Answers the Question
         * @param next The name of the Page that is supposed to follow
         * @return int Number of points gained for answer
         * */
        virtual int answer(string& next) = 0;
        virtual void printQuestion() const = 0; ///< prints the Question
        /**
         * exports a Question
         * @param outFile the stream to which the Question is exported
         * */
        virtual void exportQuestion(ofstream& outFile) const = 0;
        /**
         * imports a Question
         * @param src the stream to which the Page is imported
         * @param pg The page where the Question lies
         * @return bool return if the Question was imported
         * */
        virtual bool importQuestion(ifstream& src, Page* pg) = 0;

    protected:
        string Text = ""; ///< text of the answer itself
        bool scored = false; ///< shows if the Question gives points for answers 
        int plus_points = 0; ///< number of points added if answered correctly
        int minus_points = 0; ///< number of points substracted if answered incorrectly
        bool mandatory = false; ///< if the Question requires an answer
        Page* this_page; ///< pointer to the Page the Question is on
};