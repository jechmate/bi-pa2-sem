#include "quizselect.hpp"
#include <iostream>
#include <exception>

using namespace std;

int main (){
    //int points = 0;
    try 
    {
        QuizSelect qs;
        qs.run();
    } catch (char const* x)
    {
        cout << x << endl;
    } catch (...)
    {
        cout << "An unexpected error has occured." << endl;
    }
    return 0;
}