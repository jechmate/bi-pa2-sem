#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include "option.hpp"

ostream& operator<<(ostream& os, const Option& x){
    os << "\t\t\t" << x.position << ": " << x.text << endl;
    return os;
}

void Option::create(char pos, string option_text, Question* q, string type){ // TODO two options cant be the same
    string input;
    string following_page;
    if (pos < 'A' || pos > 'Z' || (pos > 'Z' && pos < 'a')) throw "\nNeplatna moznost.";
    this_question = q;
    position = pos;
    text = option_text;
    if (type == "choose_one")
    {
        cout << "Zadejte jmeno stranky, ktera ma nasledovat po volbe teto moznosti: (vynechejte pokud moznost nemeni poradi stran)" << endl;
        while (1)
        {
            getline(cin, following_page);
            if (cin.eof()) throw "\nPredcasne ukonceny vstup";
            if (following_page == "") break;
            cout << "Zadali jste:" << "\"" << following_page << "\"" << endl;
            cout << "Pokud si prejete zmenit jmeno stranky, napiste 'zmenit', stisknete enter pokud jste spokojeni: ";
            while (1)
            {
                getline(cin, input);
                if (cin.eof()) throw "\nPredcasne ukonceny vstup";
                if (input == "zmenit")
                {
                    cout << "Zadejte nove jmeno stranky: ";
                    break;
                }
                if (input == "")
                {
                    next_page = following_page;
                    break;
                }
                if (input != "" || input != "zmenit")
                {
                    cout << "Nespravny vstup, vynechejte nebo zadejte 'zmenit': ";
                    continue;
                }
            }
            if (input == "") 
            {
                break;
            }
        }
    }
}