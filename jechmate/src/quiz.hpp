#pragma once
#include <vector>
#include "page.hpp"

using namespace std;

class QuizSelect;
/**
 * \class Quiz
 * Represents one Quiz
 * */
class Quiz
{
    friend class Page;
    public:
        Quiz(){}; ///< default constructor
        ~Quiz(){
            for (auto v : Page_vector)
            {
                delete v;
            }
        }; ///< default destructor
        /**
         * creates the Quiz
         * @param qs the QuizSelect that holds this Quiz
         * */
        void createQuiz(QuizSelect* qs);
        /**
         * solves the Quiz
         * @return int points gained for the Page
         * */
        int solveQuiz();
        void printQuiz(); ///< prints the Quiz
        /**
         * exports a Quiz
         * @param outFile the stream to which the Quiz is exported
         * */
        void exportQuiz(ofstream& outFile) const;
        /**
         * Imports the Quiz
         * @param src the input stream of the Quiz
         * @param qs the QuizSelect that holds this Quiz
         * @return bool Returns if the Quiz was imported
         * */
        bool importQuiz(ifstream& src, QuizSelect* qs);
        string getName() const
        {
            return this->Name;
        }

    protected:
        vector<Page*> Page_vector; ///< vector of Pages this Quiz contains
        string Name = ""; ///< name of Quiz
        QuizSelect* this_quizSelect; ///< pointer to the QuizSelect object that holds this Quiz
};