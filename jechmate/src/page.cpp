#include <string>
#include <vector>
#include <iostream>
#include <algorithm>
#include <fstream>
#include "page.hpp"
#include "quiz.hpp"
#include "question.hpp"
#include "text_answer.hpp"
#include "yes_no.hpp"
#include "choose_one.hpp"
#include "choose_many.hpp"

using namespace std;

enum Question_types {
    one = 1,
    many = 2,
    yesno = 3,
    text_ans = 4
};

void Page::createPage(Quiz* qu, unsigned int page_nmr)
{
    string input;
    char question_type = 0; // 0 = incorrect, 1 = choose_one, 2 = choose_many, 3 = yes/no, 4 = text_answer
    bool found = false;
    bool pass = false;
    this_quiz = qu;
    page_number = page_nmr;
    unsigned int question_nmr = 1;
    Question* new_q = NULL;

    cout << "Zadejte nazev strany cislo " << page_number << ": ";
    while (1)
    {
        getline(cin, input);
        if (cin.eof())
        {
            throw "\nPredcasne ukonceny vstup";
            delete qu;
        }
        if (input == "")
        {
            cout << "Nazev nemuze byt prazdny." << endl;
            continue;
        } else
        {
            for (auto &v : this_quiz->Page_vector)
            {
                if (v->name == input)
                {
                    found = true;
                    break;
                }
            }
            if (found)
            {
                cout << "Stranka s timto nazvem jiz existuje, zadejte novy nazev: ";
                continue;
            } else
            {
                name = input;
                break;
            }
        }
    }
    cout << "Zadavejte otazky:" << endl;
    cout << "Typy otazek:\n\tvyber jedne moznosti\n\tvyber vice moznosti\n\tano/ne\n\ttextova odpoved" << endl;
    while (1)
    {
        cout << "Zadejte typ otazky cislo " << question_nmr << ": ";
        while (1)
        {
            getline(cin, input);
            if (cin.eof())
            {
                throw "\nPredcasne ukonceny vstup";
                delete qu;
            }
            if (input == "")
            {
                pass = true;
                break; 
            }
            transform(input.begin(), input.end(), input.begin(), ::tolower);
            if (input == "vyber jedne moznosti" || input == "vyber jedne" || input == "jedna" || input == "jedna moznost")
            {
                question_type = 1;
                break;
            } else if (input == "vyber vice moznosti" || input == "vyber vice" || input == "vice" || input == "vyber vice")
            {
                question_type = 2;
                break;
            } else if (input == "ano/ne" || input == "a/n")
            {
                question_type = 3;
                break;
            } else if (input == "textova odpoved" || input == "text" || input == "textova")
            {
                question_type = 4;
                break;
            } else 
            {
                cout << "Nespravny vstup, zadejte prosim jedno z nasledujicich:\n\tvyber jedne moznosti\n\tvyber vice moznosti\n\tano/ne\n\ttextova odpoved" << endl;
                continue;
            }
        }
        if (pass)
        {
            return;
        }
        switch (question_type)
        {
            case one :
            {
                new_q = new choose_one();
                question_vect.push_back(new_q);
                new_q->create(this);
                break;
            }
            case many :
            {
                new_q = new choose_many();
                question_vect.push_back(new_q);
                new_q->create(this);
                break;
            }
            case yesno :
            {
                new_q = new yes_no();
                question_vect.push_back(new_q);
                new_q->create(this);
                break;
            }
            case text_ans :
            {
                new_q = new text_answer();
                question_vect.push_back(new_q);
                new_q->create(this);
                break;
            }
            default : throw "\nNespravny typ otazky";
        }
        question_nmr++;
    }
}

void Page::printPage()
{
    cout << "-------------------------zacatek strany------------------------------" << endl;
    cout << "Jmeno stranky: " << name << endl;
    cout << "Cislo stranky: " << page_number << endl;
    cout << "Otazky:" << endl;
    for (auto &v : question_vect)
    {
        v->printQuestion();
    }
    cout << "--------------------------konec strany-------------------------------" << endl;
}

int Page::solvePage(string& next)
{
    string next_page = "";
    int points = 0;
    cout << "\tJmeno stranky: " << name << endl;
    cout << "\tCislo stranky: " << page_number << endl;
    cout << "\tOtazky:" << endl;
    for (auto &v : question_vect)
    {
        points += v->answer(next_page);
        if (next_page != "")
        {
            next = next_page;
        }
    }
    return points;
}

bool Page::importPage(ifstream& src, Quiz* qz, int number)
{
    string input;
    this_quiz = qz;
    Question* new_q;
    page_number = number;

    getline(src, input);
    if (input == "")
    {
        cout << "Prazdne jmeno strany." << endl;
        return false;
    }
    for (auto &v : this_quiz->Page_vector)
    {
        if (v->name == input)
        {
            cout << "V kvizu jsou dve strany se stejnym nazvem: \"" << input << "\"" << endl; 
            return false;
        }
    }
    name = input;
    getline(src, input);
    transform(input.begin(), input.end(), input.begin(), ::tolower);
    if (input != "otazky:")
    {
        cout << "Chybi vypis otazek." << endl;
        cout << "Nacteno: " << input << endl;
        return false;
    }
    getline(src, input);
    if (input != "")
    {
        cout << "Chybejici prazdna radka pred vypisem otazek na strane: \"" << name << "\"" << endl;
        cout << "Nacteno: " << input << endl;
        return false;
    }
    while (1)
    {
        getline(src, input);
        transform(input.begin(), input.end(), input.begin(), ::tolower);
        if (input == "konec strany")
        {
            getline(src, input);
            if (input != "")
            {
                cout << "Chybi prazdna radka za stranou \"" << name << "\"" << endl;
                cout << "Nacteno: " << input << endl;
                return false;
            } else return true;
        }
        if (input != "typ otazky:")
        {
            cout << "Chybejici typ otazky na strane: \"" << name << "\"" << endl;
            cout << "Nacteno: " << input << endl;
            return false;
        }
        getline(src, input);
        transform(input.begin(), input.end(), input.begin(), ::tolower);
        if (input == "vyber jedne")
        {
            new_q = new choose_one();
            if (!(new_q->importQuestion(src, this)))
            {
                delete new_q;
                return false;
            }
            question_vect.push_back(new_q);
            continue;
        } else if (input == "vyber vice")
        {
            new_q = new choose_many();
            if (!(new_q->importQuestion(src, this)))
            {
                delete new_q;
                return false;
            }
            question_vect.push_back(new_q);
            continue;
        } else if (input == "ano/ne")
        {
            new_q = new yes_no();
            if (!(new_q->importQuestion(src, this)))
            {
                delete new_q;
                return false;
            }
            question_vect.push_back(new_q);
            continue;
        } else if (input == "textova odpoved")
        {
            new_q = new text_answer();
            if (!(new_q->importQuestion(src, this)))
            {
                delete new_q;
                return false;
            }
            question_vect.push_back(new_q);
            continue;
        } else
        {
            cout << "Neplatny typ otazky na strane: \"" << name << "\"" << endl;
            cout << "Nacteno: " << input << endl;
            return false;
        }
    }
    return true;
}

void Page::exportPage(ofstream& outFile) const
{
    outFile << "Nazev strany:" << endl;
    outFile << name << endl;
    outFile << "Otazky:" << endl << endl;
    for (auto &v : question_vect)
    {
        v->exportQuestion(outFile);
    }
    outFile << "konec strany" << endl << endl;
}