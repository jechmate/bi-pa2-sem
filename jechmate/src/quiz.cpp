#include <vector>
#include <algorithm>
#include <string>
#include <fstream>
#include "quiz.hpp"
#include "quizselect.hpp"

using namespace std;

void Quiz::createQuiz(QuizSelect* qs)
{
    string input = "";
    bool found = false;
    bool pass = false;
    this_quizSelect = qs;
    Page* new_page = NULL;
    unsigned int page_number = 1;

    cout << "Zadejte nazev kvizu: ";
    while (1)
    {
        getline(cin, input);
        if (cin.eof()) throw "\nPredcasne ukonceny vstup";
        if (input == "")
        {
            cout << "Nazev nemuze byt prazdny." << endl;
            continue;
        } else
        {
            for (auto &v : this_quizSelect->quiz_vector)
            {
                if (v->Name == input)
                {
                    found = true;
                    break;
                }
            }
            if (found)
            {
                cout << "Kviz s timto nazvem jiz existuje, zadejte novy nazev: ";
                continue;
            } else
            {
                Name = input;
                break;
            }
        }
    }
    cout << "Zadavejte stranky:" << endl;
    while (1)
    {
        new_page = new Page();
        Page_vector.push_back(new_page);
        new_page->createPage(this, page_number);
        cout << "Pokud chcete zadat dalsi stranu, stisknete enter. Pokud je kviz hotov, napiste 'konec': ";
        while (1)
        {
            getline(cin, input);
            if (cin.eof())
            {
                delete new_page;
                throw "\nPredcasne ukonceny vstup";
            }
            transform(input.begin(), input.end(), input.begin(), ::tolower);
            if (input == "")
            {
                page_number++;
                pass = false;    
                break;
            } else if (input == "konec")
            {
                pass = true;
                break;
            } else {
                cout << "Nespravny vstup, zadejte 'konec', nebo stisknete enter: ";
                continue;
            }
        }
        if (pass) break;
    }
}

void Quiz::printQuiz()
{
    cout << "Jmeno kvizu: " << Name << endl;
    for (auto &v : Page_vector)
    {
        v->printPage();
    }
}

int Quiz::solveQuiz()
{
    int points = 0;
    string nextPage = "";
    auto PageIt = Page_vector.begin();

    cout << "Nazev kvizu: " << Name << endl;
    while (PageIt != Page_vector.end())
    {
        auto PageFind = Page_vector.begin();
        points += (*PageIt)->solvePage(nextPage);
        if (nextPage != "")
        {
            while (PageFind != Page_vector.end() && nextPage != (*PageFind)->getName())
            {
                ++PageFind;
            }
            if (PageFind == Page_vector.end())
            {
                cout << "Kviz ma pokracovat neexistujici stranou, preskakuji na nasledujici stranu..." << endl;
                ++PageIt;
                continue;
            } else if (PageFind == PageIt)
            {
                ++PageIt;
                continue;
            } else if (PageFind < PageIt)
            {
                //cout << "Kviz ma pokracovat stranou ktera predchazi teto, preskakuji na nasledujici stranu..." << endl;
                ++PageIt;
                continue;
            } else
            {
                PageIt = PageFind;
            }
        } else ++PageIt;
    }
    return points;
}

bool Quiz::importQuiz(ifstream& src, QuizSelect* qs)
{
    Page* new_page;
    string input;
    this_quizSelect = qs;
    int number = 1;

    getline(src, input);
    transform(input.begin(), input.end(), input.begin(), ::tolower);
    if (input != "nazev kvizu:")
    {
        cout << "Chybi nazev kvizu." << endl;
        cout << "Nacteno: " << input << endl;
        return false;
    }
    getline(src, input);
    if (input == "")
    {
        cout << "Chybi nazev kvizu." << endl;
        return false;
    }
    Name = input;
    for (auto &v : qs->quiz_vector)
    {
        if (Name == v->Name)
        cout << "Kviz s nazvem \"" << Name << "\" jiz existuje" << endl;
        return false; 
    }
    getline(src, input);
    if (input != "")
    {
        cout << "Chybi prazdna radka za nazvem kvizu." << endl;
        cout << "Nacteno: " << input << endl;
        return false;
    }
    while (1)
    {
        getline(src, input);
        transform(input.begin(), input.end(), input.begin(), ::tolower);
        if (input == "konec kvizu")
        {
            if (Page_vector.empty())
            {
                cout << "Kviz nema zadne strany, nelze jej importovat" << endl;
                return false;
            } else return true;
        }
        if (input != "nazev strany:")
        {
            cout << "Chybi nazev strany cislo " << number << endl;
            cout << "Nacteno: " << input << endl;
            return false;
        }
        new_page = new Page();
        if (!(new_page->importPage(src, this, number)))
        {
            delete new_page;
            return false;
        }
        Page_vector.push_back(new_page);
        number++;
    }
}

void Quiz::exportQuiz(ofstream& outFile) const
{
    outFile << "Nazev kvizu:" << endl;
    outFile << Name << endl << endl;
    for (auto &v : Page_vector)
    {
        v->exportPage(outFile);
    }
    outFile << "konec kvizu";
}