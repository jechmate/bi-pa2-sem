#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <fstream>
#include "question.hpp"

using namespace std;

void Question::setAtributes()
{
    string input = "";

    cout << "Zadejte otazku: ";
    while (1)
    {
        getline(cin, input);
        if (cin.eof()) throw "\nPredcasne ukonceny vstup";
        if (input == "")
        {
            cout << "Text nemuze byt prazdny" << endl;
            continue;
        } else 
        {
            Text = input;
            break;
        }
    }
    cout << "Je tato otazka bodovana? Zadejte ano/ne: ";
    while (1)
    {
        getline(cin, input);
        if (cin.eof()) throw "\nPredcasne ukonceny vstup";
        transform(input.begin(), input.end(), input.begin(), ::tolower);
        if (input != "ano" && input != "ne")
        {
            cout << "Nespravny vstup, zadejte 'ano' nebo 'ne': ";
            continue;
        } else break;
    }
    if (input == "ano")
    {
        scored = true;
        cout << "Zadejte pocet bodu za SPRAVNOU odpoved (cele cislo): ";
        while (1)
        {
            getline(cin, input);
            if (cin.eof()) throw "\nPredcasne ukonceny vstup";
            stringstream myStream(input);
            if (myStream >> plus_points) break;
            cout << "Neplatne cislo, zadejte znovu: ";
        }
        cout << "Zadejte pocet bodu za SPATNOU odpoved (cele cislo): ";
        while (1)
        {
            getline(cin, input);
            if (cin.eof()) throw "\nPredcasne ukonceny vstup";
            stringstream myStream(input);
            if (myStream >> minus_points) break;
            cout << "Neplatne cislo, zadejte znovu: ";
        }
    } else
    {
        scored = false;
    }
    cout << "Je tato otazka povinna? ";
    while (1)
    {
        getline(cin, input);
        if (cin.eof()) throw "\nPredcasne ukonceny vstup";
        transform(input.begin(), input.end(), input.begin(), ::tolower);
        if (input != "ano" && input != "ne")
        {
            cout << "Nespravny vstup, zadejte 'ano' nebo 'ne': ";
            continue;
        } else break;
    }
    if (input == "ano") mandatory = true;
}

bool Question::importAtributes(ifstream& src, Page* pg)
{
    string input;
    string substr;
    this_page = pg;
    
    getline(src, input);
    transform(input.begin(), input.end(), input.begin(), ::tolower);
    if (input != "text otazky:")
    {
        cout << "Chybi text otazky." << endl;
        return false;
    }
    getline(src, input);
    if (input == "")
    {
        cout << "Otazka s prazdnym textem, preskakuji." << endl;
        return false;
    }
    Text = input;
    getline(src, input);
    transform(input.begin(), input.end(), input.begin(), ::tolower);
    if (input != "bodovana?")
    {
        cout << "Chybi urceni bodovani u otazky \"" << Text << "\"" << endl;
        return false;
    }
    getline(src, input);
    transform(input.begin(), input.end(), input.begin(), ::tolower);
    if (input == "ano")
    {
        scored = true;
    } else if (input == "ne")
    {
        scored = false;
    }
    if (scored)
    {
        getline(src, input);
        transform(input.begin(), input.end(), input.begin(), ::tolower);
        if (input != "body za spravnou odpoved:")
        {
            cout << "Chybi urceni bodu za spravnou odpoved u otazky \"" << Text << "\"" << endl;
            return false;
        }
        getline(src, input);
        stringstream myStream1(input);
        if (!(myStream1 >> plus_points))
        {
            cout << "Neplatne cislo bodu za spravnou odpoved u otazky \"" << Text << "\"" << endl;
            return false;
        }
        getline(src, input);
        transform(input.begin(), input.end(), input.begin(), ::tolower);
        if (input != "body za spatnou odpoved:")
        {
            cout << "Chybi urceni bodu za spatnou odpoved u otazky \"" << Text << "\"" << endl;
            return false;
        }
        getline(src, input);
        stringstream myStream2(input);
        if (!(myStream2 >> minus_points))
        {
            cout << "Neplatne cislo bodu za spatnou odpoved u otazky \"" << Text << "\"" << endl;
            return false;;
        }
    }
    getline(src, input);
    transform(input.begin(), input.end(), input.begin(), ::tolower);
    if (input != "povinna?")
    {
        cout << "Chybi urceni povinnosti u otazky \"" << Text << "\"" << endl;
        return false;
    }
    getline(src, input);
    transform(input.begin(), input.end(), input.begin(), ::tolower);
    if (input == "ano")
    {
        mandatory = true;
    } else if (input == "ne")
    {
        mandatory = false;
    } else
    {
        cout << "Nespravny vstup povinnosti u otazky \"" << Text << "\"" << endl;
        return false;
    }
    return true;
}

void Question::exportAtributes(ofstream& outFile) const
{
    outFile << "Text otazky:" << endl;
    outFile << Text << endl;
    outFile << "Bodovana?" << endl;
    if (scored) outFile << "ano" << endl;
    else outFile << "ne" << endl;
    if (scored)
    {
        outFile << "Body za spravnou odpoved:" << endl;
        outFile << plus_points << endl;
        outFile << "Body za spatnou odpoved:" << endl;
        outFile << minus_points << endl;
    }
    outFile << "Povinna?" << endl;
    if (mandatory) outFile << "ano" << endl;
    else outFile << "ne" << endl;
}