#include "choose_one.hpp"
#include <algorithm>
#include <fstream>

void choose_one::create(Page* pg)
{
    string input;
    string option_text;
    string type = "choose_one";
    bool pass = false;
    bool incorrect;
    Option opt;
    char option = 'A';
    this_page = pg;

    this->setAtributes();
    cout << "Zadejte moznosti:" << endl;
    while (1)
    {
        incorrect = false;
        cout << "Moznost " << option << ": ";
        getline(cin, option_text);
        if (cin.eof()) throw "\nPredcasne ukonceny vstup";
        if (option_text == "")
        {
            pass = true;
        } else 
        {
            for (auto &v : options)
            {
                if (v.getText() == option_text)
                {
                    incorrect = true;
                    break;
                }
            }
        }
        if (incorrect)
        {
            cout << "Tato moznost jiz existuje, zadejte prosim novou." << endl;
            continue;
        }
        if (!pass)
        {
            opt.create(option, option_text, this, type);
        }
        if (pass && (option == 'A' || option == 'B'))
        {
            cout << "Musite zadat alespon dve moznosti." << endl;
            pass = false;
        } else
        if (pass || option == 'Z')
        {
            if (pass) option--;
            if (option == 'Z') options.push_back(Option(opt));
            if (scored)
            {
                cout << "Zadejte spravnou moznost (A-" << option << "): ";
                while (1)
                {
                    getline(cin, input);
                    if (cin.eof()) throw "\nPredcasne ukonceny vstup";
                    if (input.length() > 1)
                    {
                        cout << "Zadejte prosim pouze jeden znak: ";
                        continue;
                    }
                    transform(input.begin(), input.end(), input.begin(), ::toupper);
                    correct_option = input[0];
                    if (correct_option < 'A' || (correct_option > option ))
                    {
                        cout << "Nespravny vstup, zadejte (A-" << option << "): ";
                        continue;
                    }
                    break;
                }
            }
            break;
        } else 
        {
            options.push_back(Option(opt));
            pass = false;
            option++;
        }
    }
}

void choose_one::printQuestion() const
{
    if (Text == "") throw "\nNeplatna otazka.";
    cout << "\t\tOtazka: " << Text << endl;
    for (auto &v : options)
    {
        cout << v;
    }
}

int choose_one::answer(string& next)
{
    
    string input;
    char maxOption = options.size() + 64;
    char option;
    bool passed = false;
    if (maxOption <= 65 || maxOption > 90) throw "\nNeplatna otazka.";
    this->printQuestion();
    cout << "\t\tOdpoved (zadejte znak A-" << maxOption << "): ";
    while (1)
    {
        getline(cin, input);
        if (cin.eof()) throw "\nPredcasne ukonceny vstup.";
        if (input.length() == 0)
        {
            if (mandatory)
            {
                cout << "\t\tOtazka je povinna, musite zadat odpoved: ";
                continue;
            } else
            {           
                cout << "Preskakuji odpoved..." << endl;
                passed = true;
                break;
            }
        }
        if (input.length() > 1)
        {
            cout << "\t\tZadejte prosim pouze jeden znak: ";
            continue;
        }
        transform(input.begin(), input.end(), input.begin(), ::toupper);
        option = input[0];
        if (option < 'A' || option > maxOption)
        {
            cout << "\t\tNespravny vstup, zadejte znovu: ";
            continue;
        } else
        {
            if (!passed)
            {
                next = options[option-65].getNext();
                break;
            } else next = "";
        }
    }
    if (scored && !passed)
    {
        if (option == correct_option)
        {
            cout << "\t\tSpravne, ziskavate " << plus_points << " bodu." << endl;
            return plus_points;
        } else
        {
            cout << "\t\tSpatne, ziskavate " << minus_points << " bodu." << endl;
            return minus_points;
        }
    }
    return 0;
}

bool choose_one::importQuestion(ifstream& src, Page* pg)
{
    if (!importAtributes(src, pg)) return false;
    string input;
    char position = 'A';
    string option_text = "";
    string next_page = "";
    bool skip = false;

    getline(src, input);
    transform(input.begin(), input.end(), input.begin(), ::tolower);
    if (input != "moznosti:")
    {
        cout << "Chybi vypis moznosti u otazky \"" << Text << "\"" << endl;
        cout << "Nacteno: " << input << endl;
        return false;
    }
    while (1)
    {
        getline(src, input);
        transform(input.begin(), input.end(), input.begin(), ::tolower);
        if (input == "konec moznosti")
        {
            if (position > 'B') break;
            else
            {
                cout << "Otazka \"" << Text << "\" ma mene nez dve moznosti." << endl;
                return false;
            }
        }
        if ( position == ('Z' + 1))
        {
            cout << "Otazka \"" << Text << "\" ma prilis mnoho moznosti." << endl;
            return false;
        }
        if (input != "text:")
        {
            cout << "Chybi text moznosti u otazky \"" << Text << "\"" << endl;
            cout << "Nacteno: " << input << endl;
            return false;
        }
        getline(src, input);
        if (input == "")
        {
            cout << "Chybi text moznosti u otazky \"" << Text << "\"" << endl;
            cout << "Nacteno: " << input << endl;
            return false;
        }
        for (auto &v : options)
        {
            if (v.getText() == input)
            {
                skip = true;
                break;
            }
        }
        option_text = input;
        getline(src, input);
        transform(input.begin(), input.end(), input.begin(), ::tolower);
        if (input != "pristi strana:")
        {
            cout << "Chybi zadani pristi strany u otazky \"" << Text << "\"" << endl;
            return false;
        }
        getline(src, next_page);
        if (skip)
        {
            cout << "Zadana stejna moznost vicekrat, preskakuji ji." << endl;
            skip = false;
            continue;
        } else
        {
            options.push_back(Option(position, option_text, next_page, this));
            position++;   
        }
    }
    position--;
    if (scored)
    {
        getline(src, input);
        transform(input.begin(), input.end(), input.begin(), ::tolower);
        if (input != "spravna odpoved:")
        {
            cout << "Chybi vypis spravne odpovedi u otazky \"" << Text << "\"" << endl;
            cout << "Nacteno: " << input << endl;
            return false;
        }
        getline(src, input);
        if (input.length() > 1)
        {
            cout << "Spravna odpoved neni jeden znak u otazky \"" << Text << "\"" << endl;
            cout << "Nacteno: " << input << endl;
            return false;
        }
        transform(input.begin(), input.end(), input.begin(), ::toupper);
        correct_option = input[0];
        if (correct_option < 'A' || (correct_option > position ))
        {
            cout << "Nespravna spravna moznost u otazky \"" << Text << "\"" << endl;
            cout << "Nacteno: " << input << endl;
            return false;
        }
    }
    getline(src, input);
    transform(input.begin(), input.end(), input.begin(), ::tolower);
    if (input != "konec otazky")
    {
        cout << "Chybi konec otazky u otazky \"" << Text << "\"" << endl;
        cout << "Nacteno: " << input << endl;
        return false;
    }
    getline(src, input);
    transform(input.begin(), input.end(), input.begin(), ::tolower);
    if (input != "")
    {
        cout << "Chybi prazdna radka za otazkou \"" << Text << "\"" << endl;
        cout << "Nacteno: " << input << endl;
        return false;
    }
    return true;
}

void choose_one::exportQuestion(ofstream& outFile) const
{
    outFile << "Typ otazky:" << endl;
    outFile << "vyber jedne" << endl;
    this->exportAtributes(outFile);
    outFile << "Moznosti:" << endl;
    for (auto &v : options)
    {
        outFile << "Text:" << endl;
        outFile << v.getText() << endl;
        outFile << "Pristi strana:" << endl;
        outFile << v.getNext() << endl;
    }
    outFile << "konec moznosti" << endl;
    if (scored)
    {
        outFile << "Spravna odpoved:" << endl;
        outFile << correct_option << endl;
    }
    outFile << "konec otazky" << endl << endl;
}