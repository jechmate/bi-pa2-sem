#pragma once
#include <stdio.h>
#include "quiz.hpp"

using namespace std;

/**
 * \class QuizSelect
 * Used for holding all Quizes, solving them and creating new ones or importing/exporting them
 * from/to a file
 * */
class QuizSelect
{
    friend class Quiz;
    public:
        QuizSelect(){}; ///< default constructor
        ~QuizSelect()
        {
            for (auto v : quiz_vector)
            {
                delete v;
            }
        }; ///< default destructor
        void run(); ///< run the app, wait for user input
        void createQuiz(); ///< creates a new quiz
        /**
         * Solves the given Quiz
         * @param q the Quiz to be solved
         * @return int Return the number of points gained after completing the Quiz
         * */
        int solveQuiz(Quiz& q);
        void importQuiz(); ///< imports a new Quiz from a file
        /**
         * Exports the given Quiz
         * @param q the Quiz to be exported
         * */
        void exportQuiz(const Quiz& q);

    protected:
        vector<Quiz*> quiz_vector; ///< vector that holds all Quizes
};