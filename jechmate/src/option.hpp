#pragma once
#include <iostream>
#include <string>

using namespace std;

class Question;
/**
 * \class Option
 * Represents one option in a given Question
 * */
class Option
{
    public:
        Option(){}; ///< default constructor
        Option(char position, string text, string next_page, Question* this_question) : 
        position(position), text(text), next_page(next_page), this_question(this_question){};
        ~Option(){}; ///< default destructor
        Option(const Option& x)
        {
            next_page = x.next_page;
            position = x.position;
            text = x.text;
            this_question = x.this_question;
        };
        /**
         * Creates the option
         * @param position The symbol of the option to create
         * @param text The text of the option to create
         * @param q A pointer to the Question where the option lies
         * @param type Type of the Question where the Option lies
         * */
        void create(char position, string text, Question* q, string type);
        string getNext() const
        {
            return next_page;
        }
        char getPosition() const
        {
            return position;
        }
        string getText() const
        {
            return text;
        }
        friend ostream& operator<<(ostream& os, const Option& x);
        Option& operator=(const Option x)
        {
            next_page = x.next_page;
            position = x.position;
            text = x.text;
            this_question = x.this_question;
            return *this;
        }

    protected:
        char position; ///< Symbol of the answer. A,B,...,Z
        string text; ///< Text of the Option
        string next_page = ""; ///< if the Option is selected, this is the name of the Page to follow. If blank, Page is taken sequentially
        Question* this_question; ///< the Question in which this Option lies
};