#pragma once
#include "question.hpp"

/**
 * \class text_answer
 * Question where you have to type in the answer
 * */
class text_answer : public Question
{
    public:
        text_answer(){};
        ~text_answer(){};
        text_answer(const text_answer& src){
            this->Text = src.Text;
            this->scored = src.scored;
            this->plus_points = src.plus_points;
            this->minus_points = src.minus_points;
            this->this_page = src.this_page;
            this->correct_options = src.correct_options;
            this->user_answer = src.user_answer;
        }
        void create(Page*);
        int answer(string&);
        void printQuestion() const;
        bool importQuestion(ifstream&, Page*);
        void exportQuestion(ofstream&) const;

    private:
        set<string> correct_options; ///< Set of correct answers, can also be multiple variations of one answer (lower/upper case)
        map<string,string> change_options; ///< Map of options that change the next Page, Second = name of the Page to follow
        string user_answer = ""; ///< answer the user gave
};