#include "quizselect.hpp"
#include <algorithm>
#include <dirent.h>
#include <fstream>

using namespace std;

void QuizSelect::run(){
    string input = "";
    int points = 0;

    cout << "Vitejte v manazeru kvizu" << endl;
    while (1)
    {
        points = 0;
        cout << "Zadejte co chcete delat ('pomoc' pro vypsani prikazu): ";
        getline (cin, input);
        if (cin.eof()) throw "\nPredcasne ukonceny vstup";
        transform(input.begin(), input.end(), input.begin(), ::tolower);
        if (input == "vypsat kvizy" || input == "vypsat")
        {
            if (quiz_vector.empty())
            {
                cout << "Nemate zadne kvizy, muzete nejaky vytvorit nebo importovat." << endl;
                continue;
            }
            for (auto &v : quiz_vector)
            {
                cout << "\t" << v->getName() << endl;
            }
            cout << "Zadejte dalsi akci: " << endl;
            continue;
        } else if (input == "vytvorit" || input == "vytvorit kviz")
        {
            createQuiz();
            continue;
        } else if (input == "vyresit kviz" || input =="vyresit")
        {
            if (quiz_vector.empty())
            {
                cout << "Nemate zadne kvizy, muzete nejaky vytvorit nebo importovat." << endl;
                continue;
            }
            cout << "Zadejte nazev kvizu, ktery chcete resit: ";
            getline (cin, input);
            if (cin.eof()) throw "\nPredcasne ukonceny vstup";
            auto QuizIt = quiz_vector.begin();
            while (1)
            {
                if (QuizIt == quiz_vector.end())
                {
                    cout << "Kviz s timto jmenem neexistuje, zadejte novou akci: ";
                    break;
                }
                if (input == (*QuizIt)->getName())
                {
                    points = solveQuiz(**QuizIt);
                    cout << "Hodnoceni:\n\tNazev kvizu: " << (*QuizIt)->getName() << "\n\tPocet bodu: " << points << endl;
                    break;
                }
                ++QuizIt;
            }
            continue;
        } else if (input == "exportovat kviz" || input == "exportovat" || input == "export")
        {
            if (quiz_vector.empty())
            {
                cout << "Nemate zadne kvizy, muzete nejaky vytvorit nebo importovat." << endl;
                continue;
            }
            cout << "Zadejte nazev kvizu k exportu: ";
            getline (cin, input);
            if (cin.eof()) throw "\nPredcasne ukonceny vstup";
            auto QuizIt = quiz_vector.begin();
            while (1)
            {
                if (QuizIt == quiz_vector.end())
                {
                    cout << "Kviz s timto jmenem neexistuje, zadejte novou akci." << endl;
                    break;
                }
                if (input == (*QuizIt)->getName())
                {
                    exportQuiz(**QuizIt);
                    break;
                }
                ++QuizIt;
            }
            continue;
        } else if (input == "importovat kviz" || input == "importovat" || input == "import")
        {
            importQuiz();
            continue;
        } else if (input == "konec" || input == "ukoncit" || input == "ukoncit program")
        {
            cout << "Ukoncuji program..." << endl;
            break;
        } else if (input == "pomoc" || input == "help")
        {
            cout << "\tvypsat kvizy\n\tvyresit kviz\n"
            "\tvytvorit kviz\n\texportovat kviz\n\timportovat kviz\n\tukoncit program" << endl;
            continue;
        } else
        {
            cout << "Nespravny vstup." << endl;
            continue;
        }
    }
}

void QuizSelect::createQuiz()
{
    Quiz* newQuiz = nullptr;
    newQuiz = new Quiz();
    quiz_vector.push_back(newQuiz);
    newQuiz->createQuiz(this);
}

int QuizSelect::solveQuiz(Quiz& qz)
{
    int points = 0;
    points += qz.solveQuiz();
    return points;
}

void QuizSelect::importQuiz()
{
    Quiz* newQuiz = nullptr;
    DIR *dir;
    struct dirent *ent;
    string input;
    string examplesDir = "examples/";

    if ((dir = opendir ("examples")) != NULL)
    {
        cout << "Soubory: " << endl;
        while ((ent = readdir (dir)) != NULL)
        {
            if( ent->d_type == DT_REG )
            {
                cout << "\t" << ent->d_name << endl;
            }
        }
        closedir (dir);
    } else {
        cout << "Nepodarilo se otevrit slozku 'examples'" << endl;
        return;
    }
    cout << "Zadejte nazev souboru k importu: ";
    getline (cin, input);
    if (cin.eof()) throw "\nPredcasne ukonceny vstup";
    if (input == "")
    {
        cout << "Zpet do menu..." << endl;
        return;
    }
    examplesDir.append(input);
    ifstream infile(examplesDir);
    if (infile.is_open())
    {
    } else cout << "Nepodarilo se otevrit soubor \"" << input << "\"" << endl;
    newQuiz = new Quiz();
    if (newQuiz->importQuiz(infile, this))
    {
        cout << "Kviz \"" << input << "\" byl importovan." << endl;
    } else
    {
        delete newQuiz;
        return;
    }
    quiz_vector.push_back(newQuiz);
    infile.close();
}

void QuizSelect::exportQuiz(const Quiz& qz)
{
    string filename;
    DIR *dir;
    struct dirent *ent;
    bool found = false;
    string input;
    string examplesDir = "examples/";
    
    cout << "Zadejte nazev souboru do ktereho budete exportovat: ";
    while (1)
    {
        getline(cin, input);
        if (input == "")
        {
            cout << "Nazev souboru nemuze byt prazdny, zadejte znovu: ";
            continue;
        }
        if ((dir = opendir ("examples")) != NULL)
        {
            while ((ent = readdir (dir)) != NULL)
            {
                if (ent->d_name == input)
                {
                    found = true;
                    break;
                }
            }
            closedir (dir);
            if (found)
            {
                cout << "Soubor s timto nazvem jiz existuje, zadejte novy nazev: ";
                found = false;
                continue;
            } else break;
        } else {
            cout << "Nepodarilo se otevrit slozku 'examples'" << endl;
            return;
        }
    }
    examplesDir.append(input);
    ofstream outFile(examplesDir); // TODO neplatny nazev souboru
    qz.exportQuiz(outFile);
}