#pragma once
#include <vector>
#include "question.hpp"
/**
 * \class choose_one
 * Question where you can choose only one answer can be answered 
 * by typing in the character marking the Option
 * */
class choose_one : public Question
{
    public:
        choose_one(){};
        ~choose_one(){};
        choose_one(const choose_one& src){
            this->Text = src.Text;
            this->scored = src.scored;
            this->plus_points = src.plus_points;
            this->minus_points = src.minus_points;
            this->this_page = src.this_page;
            this->options = src.options;
            this->correct_option = src.correct_option;
            this->user_answer = src.user_answer;
        }
        void create(Page*);
        int answer(string&);
        void printQuestion() const;
        bool importQuestion(ifstream&, Page*);
        void exportQuestion(ofstream&) const;

    private:
        vector<Option> options; ///< Vector of options given to the user
        char correct_option; ///< char of the correct option
        char user_answer; ///< char of the user's answer 
};