#pragma once
#include <vector>
#include "question.hpp"

using namespace std;

class Quiz;
/**
 * \class Page
 * Represents one page in the Quiz
 * */
class Page
{
    public:
        Page(){}; ///< default constructor
        ~Page()
        {
            for (auto v : question_vect)
            {
                delete v;
            }
        }; ///< default destructor
        Page(const Page& src){
            this->question_vect = src.question_vect;
            this->name = src.name;
            this->points = src.points;
            this->this_quiz = src.this_quiz;
            this->page_number = src.page_number;
        } ///< copy constructor
        /**
         * creates a Question on this Page
         * @param qz pointer to the Quiz where the Page lies
         * @param number The number of the Page
         * */
        void createPage(Quiz* qz, unsigned int number);
        /**
         * solves a Question on this Page
         * @param next The Page that is supposed to follow this one in the Quiz, defaults to the next in vector if null
         * @return int Returns the number of points gained for the Question
         * */
        int solvePage(string& next);
        void printPage(); ///< prints the whole Page
        /**
         * exports a Page
         * @param outFile The stream to which the Page is exported
         * */
        void exportPage(ofstream& outFile) const;
        string getName() ///< returns the name of the Page
        {
            return this->name;
        };
        /**
         * imports a Page
         * @param src the stream from which the Page is imported
         * @param qz the Quiz in which the Page lies
         * @param number the number of the Page
         * @return bool Returns if the Page was imported
         * */
        bool importPage(ifstream& src, Quiz* qz, int number);

    protected:
        vector<Question*> question_vect; ///< Vector of Questions on this Page
        string name; ///< Name of the Page, can be used for identification
        int points = 0; ///< Number of points gained for solving this Page
        Quiz* this_quiz; ///< the Quiz in which this Page lies
        unsigned int page_number; ///< number of the Page in the Quiz
};