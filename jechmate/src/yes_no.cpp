#include "yes_no.hpp"
#include <algorithm>
#include <sstream>
#include <fstream>

void yes_no::create(Page* pg)
{
    string input;
    string next;
    this_page = pg;

    this->setAtributes();
    if (scored)
    {
        cout << "Zadejte spravnou moznost (ano/ne): ";
        while (1)
        {
            getline(cin, input);
            if (cin.eof()) throw "\nPredcasne ukonceny vstup";
            transform(input.begin(), input.end(), input.begin(), ::toupper);
            if (input == "ANO" || input == "PRAVDA" || input == "JO" || input == "A")
            {
                correct_option = true;
                break;
            } else if (input == "NE" || input == "NEPRAVDA" || input == "LEZ" || input == "N")
            {
                correct_option = false;
                break;
            } else
            {
                cout << "Nespravny vstup, zadejte ano/ne: ";
                continue;
            }
        }
    }
    cout << "Meni tato otazka poradi stranek? (zadejte ano/ne): ";
    while (1)
    {
        getline(cin, input);
        if (cin.eof()) throw "\nPredcasne ukonceny vstup";
        transform(input.begin(), input.end(), input.begin(), ::toupper);
        if (input == "ANO" || input == "PRAVDA" || input == "JO" || input == "A")
        {
            cout << "Zadejte, ktera strana ma nasledovat po 'ano' (vynechejte, pokud moznost nemeni poradi stran): ";
            while (1)
            {
                getline(cin, next);
                if (cin.eof()) throw "\nPredcasne ukonceny vstup";
                if (next == "")
                {
                    break;
                }
                cout << "Zadali jste:" << "\"" << next << "\"" << endl;
                cout << "Pokud si prejete zmenit jmeno stranky, napiste 'zmenit', stisknete enter pokud jste spokojeni: ";
                while (1)
                {
                    getline(cin, input);
                    if (cin.eof()) throw "\nPredcasne ukonceny vstup";
                    if (input == "zmenit")
                    {
                        cout << "Zadejte nove jmeno stranky, neo vynechejte, pokud moznost nemeni poradi stran: ";
                        break;
                    }
                    if (input == "")
                    {
                        break;
                    }
                    if (input != "" || input != "zmenit")
                    {
                        cout << "Nespravny vstup, vynechejte nebo zadejte 'zmenit': ";
                        continue;
                    }
                }
                if (input == "") 
                {
                    next_page_true = next;
                    break;
                }
            }
            cout << "Zadejte, ktera strana ma nasledovat po 'ne' (vynechejte, pokud moznost nemeni poradi stran): ";
            while (1)
            {
                getline(cin, next);
                if (cin.eof()) throw "\nPredcasne ukonceny vstup";
                if (next == "")
                {
                    break;
                }
                cout << "Zadali jste:" << "\"" << next << "\"" << endl;
                cout << "Pokud si prejete zmenit jmeno stranky, napiste 'zmenit', stisknete enter pokud jste spokojeni: ";
                while (1)
                {
                    getline(cin, input);
                    if (cin.eof()) throw "\nPredcasne ukonceny vstup";
                    if (input == "zmenit")
                    {
                        cout << "Zadejte nove jmeno stranky, nebo vynechejte, pokud moznost nemeni poradi stran: ";
                        break;
                    }
                    if (input == "")
                    {
                        break;
                    }
                    if (input != "" || input != "zmenit")
                    {
                        cout << "Nespravny vstup, vynechejte nebo zadejte 'zmenit': ";
                        continue;
                    }
                }
                if (input == "") 
                {
                    next_page_false = next;
                    break;
                }
            }
        } else if (input == "NE" || input == "NEPRAVDA" || input == "LEZ" || input == "N")
        {
            break;
        } else
        {
            cout << "Nespravny vstup, zadejte ano/ne: ";
            continue;
        }
        break;   
    }
}

void yes_no::printQuestion() const
{
    if (Text == "") throw "\nNeplatna otazka.";
    cout << "\t\tOtazka: " << Text << endl;
}

int yes_no::answer(string& next)
{
    user_answer = false;
    string input;
    this->printQuestion();
    bool answer = false;
    bool passed = false;
    cout << "\t\tOdpoved (zadejte ano/ne): ";
    while (1)
    {
        getline(cin, input);
        if (cin.eof()) throw "\nPredcasne ukonceny vstup.";
        if (input.length() == 0)
        {
            if (mandatory)
            {
                cout << "Otazka je povinna, musite zadat odpoved: ";
                continue;
            } else
            {           
                cout << "Preskakuji odpoved..." << endl;
                passed = true;
                break;
            }
        }
        transform(input.begin(), input.end(), input.begin(), ::toupper);
        if (input == "ANO" || input == "PRAVDA" || input == "JO" || input == "A")
        {
            answer = true;
            break;
        } else if (input == "NE" || input == "NEPRAVDA" || input == "LEZ" || input == "N")
        {
            answer = false;
            break;
        } else
        {
            cout << "\t\tNespravny vstup, zadejte ano/ne: ";
            continue;
        }
    }
    if (!passed)
    {
        if (answer) next = next_page_true;
        else next = next_page_false;
    } else next = "";
    if (scored && !passed)
    {
        if (answer == correct_option)
        {
            cout << "\t\tSpravne, ziskavate " << plus_points << " bodu." << endl;
            return plus_points;
        } else
        {
            cout << "\t\tSpatne, ziskavate " << minus_points << " bodu." << endl;
            return minus_points;
        }
    }
    return 0;
}

bool yes_no::importQuestion(ifstream& src, Page* pg)
{
    if (!importAtributes(src, pg)) return false;
    string input;
    if (scored)
    {
        getline(src, input);
        transform(input.begin(), input.end(), input.begin(), ::tolower);
        if (input != "spravna moznost:")
        {
            cout << "Chybi spravna moznost u otazky \"" << Text << "\"" << endl;
            cout << "Nacteno: " << input << endl;
            return false;
        }
        getline(src, input);
        transform(input.begin(), input.end(), input.begin(), ::tolower);
        if (input == "")
        {
            cout << "Chybi spravna moznost u otazky \"" << Text << "\"" << endl;
            return false;
        } else if (input == "ano") correct_option = true;
        else if (input == "ne") correct_option = false;
        else
        {
            cout << "Spatne zadana spravna moznost u otazky \"" << Text << "\"" << endl;
            cout << "Nacteno: " << input << endl;
            return false;
        }
    }
    getline(src, input);
    transform(input.begin(), input.end(), input.begin(), ::tolower);
    if (input != "meni poradi stran?")
    {
        cout << "Chybi zmena poradi stran u otazky \"" << Text << "\"" << endl;
        cout << "Nacteno: " << input << endl;
        return false;
    }
    getline(src, input);
    transform(input.begin(), input.end(), input.begin(), ::tolower);
    if (input == "ano")
    {
        getline(src, input);
        transform(input.begin(), input.end(), input.begin(), ::tolower);
        if (input != "strana po ano:")
        {
            cout << "Chybi zmena poradi po ano u otazky \"" << Text << "\"" << endl;
            cout << "Nacteno: " << input << endl;
            return false;
        }
        getline(src, input);
        if (input == "")
        {
            cout << "Chybi zmena poradi po ano u otazky \"" << Text << "\"" << endl;
            return false;
        }
        next_page_true = input;
        getline(src, input);
        transform(input.begin(), input.end(), input.begin(), ::tolower);
        if (input != "strana po ne:")
        {
            cout << "Chybi zmena poradi po ne u otazky \"" << Text << "\"" << endl;
            cout << "Nacteno: " << input << endl;
            return false;
        }
        getline(src, input);
        if (input == "")
        {
            cout << "Chybi zmena poradi po ne u otazky \"" << Text << "\"" << endl;
            return false;
        }
        next_page_false = input;
    } else if (input != "ne")
    {
        cout << "Nespravna odpoved na zmenu poradi stran u otazky \"" << Text << "\"" << endl;
        cout << "Nacteno: " << input << endl;
        return false;
    }
    getline(src, input);
    transform(input.begin(), input.end(), input.begin(), ::tolower);
    if (input != "konec otazky")
    {
        cout << "Chybi konec otazky u otazky \"" << Text << "\"" << endl;
        cout << "Nacteno: " << input << endl;
        return false;
    }
    getline(src, input);
    transform(input.begin(), input.end(), input.begin(), ::tolower);
    if (input != "")
    {
        cout << "Chybi prazdna radka za otazkou \"" << Text << "\"" << endl;
        cout << "Nacteno: " << input << endl;
        return false;
    }
    return true;
}

void yes_no::exportQuestion(ofstream& outFile) const
{
    outFile << "Typ otazky:" << endl;
    outFile << "ano/ne" << endl;
    this->exportAtributes(outFile);
    if (scored)
    {
        outFile << "Spravna moznost:" << endl;
        if (correct_option) outFile << "ano" << endl;
        else outFile << "ne" << endl;
    }
    outFile << "Meni poradi stran?" << endl;
    if (next_page_false != "" || next_page_true != "")
    {
        outFile << "ano" << endl;
    } else outFile << "ne" << endl;
    if (next_page_false != "" || next_page_true != "")
    {
        outFile << "Strana po ano:" << endl;
        outFile << next_page_true << endl;
        outFile << "Strana po ne:" << endl;
        outFile << next_page_false << endl;
    }
    outFile << "konec otazky" << endl << endl;
}